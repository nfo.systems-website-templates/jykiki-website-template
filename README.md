# Jykiki Web Template

HTML template for basic websites, based on [Bootstrap](https://getbootstrap.com/) 5. 

## About

The Jykiki template is open source. The HTML, CSS and JavaScript files are released under the [MIT license](LICENSE).

Jykiki, a web template created and maintained by NFO.systems.

![Jykiki free website template](nfoSystemsJykiki.jpeg "Jykiki free website template")

- https://nfo.systems/
- https://gitlab.com/nfo.systems-website-templates/

## Download

- [zip](https://gitlab.com/nfo.systems-website-templates/)
- Clone it with Git: <code>https://gitlab.com/nfo.systems-website-templates/jykiki-website-template.git</code>

## Copyright and License

Copyright 2013-2022 NFO Systems S.A.P.I. de C.V.

HTML, CSS and JS code released under the MIT license.

*Uploading-bro.svg* and *Shared workspace-bro.svg* images are owned and licensed by [Storyset](https://storyset.com/) that requires you to publish attribution when using their images.